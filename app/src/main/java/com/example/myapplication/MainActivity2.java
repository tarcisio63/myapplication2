package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
// This is the Sign Up Activity, where it saves credentials of a new client to the database
public class MainActivity2 extends AppCompatActivity {
    private Button button2;
    private EditText nameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        button2 = (Button) findViewById(R.id.button2);
        nameEditText = findViewById(R.id.name);
        emailEditText = findViewById(R.id.email);
        passwordEditText = findViewById(R.id.password);
        //Button SignUP .....It gets the data from the Edittext via getText();
        button2.setOnClickListener(v -> {
            String nameValue = nameEditText.getText().toString();
            String emailValue = emailEditText.getText().toString();
            String passwordValue = passwordEditText.getText().toString();
//User to insert credentials.
            ParseUser user = new ParseUser();
            user.setUsername(nameValue);
            user.setEmail(emailValue);
            user.setPassword(passwordValue);
//Saving the data to the database
            Log.d( "Username: ", nameValue);
            Log.d("Email: ", emailValue);
            Log.d("Password: ", passwordValue);

            user.signUpInBackground(new SignUpCallback() {
               @Override
                public void done(ParseException e) {
                    if (e == null) {
                        openActivity();
                        // Show a simple Toast message upon successful registration
                        Toast.makeText(getApplicationContext(),
                                "Successfully Signed up, please log in.",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Sign up Error", Toast.LENGTH_LONG)
                                .show();
                    }
                }
            });
        });
    }
// It calls Log In class.
    private void openActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}