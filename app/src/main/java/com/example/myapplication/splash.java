package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
//Splash Activity.
public class splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //If successful
                openActivity();

            }
        }, 5000);// These are the milliseconds its supposed to take to get to the new activity.
    }

    private void openActivity() {
        Intent intent =  new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
