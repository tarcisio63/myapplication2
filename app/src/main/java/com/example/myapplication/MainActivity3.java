package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
//Forgot password class.
public class MainActivity3 extends AppCompatActivity {
    private Button forgot;
    private EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        email= findViewById(R.id.email);
        forgot = findViewById(R.id.forgot);
        //It gets the email and successfully sends instructions.
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailValue = email.getText().toString();
                ParseUser.requestPasswordResetInBackground("myemail@example.com", new RequestPasswordResetCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            // An email was successfully sent with reset instructions.
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Check your mail", Toast.LENGTH_LONG)
                                    .show();
                            // Something went wrong. Look at the ParseException to see what's up.
                        }
                    }
                });
                ParseUser.requestPasswordResetInBackground(
                        emailValue,
                        new RequestPasswordResetCallback() {
                            public void done(ParseException e) {

                            }
                        }
                );
            }
        });
    }
}
