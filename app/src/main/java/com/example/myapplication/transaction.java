package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.DragAndDropPermissions;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static java.lang.String.valueOf;

public class transaction extends AppCompatActivity {
    private Button add;
    private SwipeRefreshLayout swiperefresh;
    private EditText num2;
    private TextView text;
    private TextView show;
    private Button negate;
    private ParseObject value;
    private int i;
    private TextView user1;
    private TextView show1;
    private TextView show2;
    private TextView show3;
    private TextView object1;
    public transaction() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        swiperefresh = findViewById(R.id.swiperefresh);
        user1 = findViewById(R.id.user1);
        negate = findViewById(R.id.negate);
        show = findViewById(R.id.show);
        show1 = findViewById(R.id.show1);
        show2 = findViewById(R.id.show2);
        show3 = findViewById(R.id.show3);
        object1 = findViewById(R.id.object1);
        num2 = findViewById(R.id.num2);
        text = findViewById(R.id.text);
        add = findViewById(R.id.add);
        //It refresh to pull the new data from the database.
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getDataFromServer();
                        swiperefresh.setRefreshing(false);
                    }
                }
        );
        //  top up instruction code.
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numb1 = Integer.parseInt(show.getText().toString()); //By default is the current Balance.
                int numb2 = Integer.parseInt(num2.getText().toString());
                int sum = numb1 + numb2;
                text.setText(String.valueOf(sum));
                //It checks if the values inserted are integers
                if (show.getText().toString().length() == 0) {
                    show.setText("0");
                }
                if (num2.getText().toString().length() == 0) {
                    num2.setText("0");
                }
                //Stores the new data after a successful transaction to the database.
                ParseObject trial = new ParseObject("trial");
                trial.put("credit", String.valueOf(numb2));// It stores the current amount topped up
                trial.put("debit", "0");
                trial.put("trial", String.valueOf(sum)); // It adds the current amount from the intial balance.
                trial.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            ParseUser.logOut();
                            ParseUser currentUser = ParseUser.getCurrentUser();
                        }
                    }
                });
            }
        });
        //Withdraw instruction code.
        negate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numb1 = Integer.parseInt(show.getText().toString());//By default is the current Balance.
                int numb2 = Integer.parseInt(num2.getText().toString());//Enter amount one wants to withdraw.
                int negate = numb1 - numb2;
                //It checks if the values inserted are integers
                text.setText(String.valueOf(negate));
                if (show.getText().toString().length() == 0) {
                    show.setText("0");
                }
                if (num2.getText().toString().length() == 0) {
                    num2.setText("0");
                }
                ParseObject trial = new ParseObject("trial");
                trial.put("debit", String.valueOf(numb2));//It pushes the current amount withdrawn.
                trial.put("credit", "0");
                trial.put("trial", String.valueOf(negate));//It subtracts the current amount from the intial balance.
                trial.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            //
                        } else {
                        }

                    }
                });
            }
        });
        //Fetches the current user logged to the app.
        ParseUser currentUser = ParseUser.getCurrentUser();
        if(currentUser != null){
            String username = currentUser.getUsername();
            user1.setText(username);
        }
        getDataFromServer();
    }

    private void getDataFromServer() {
        //Queries data to get the most recent transaction made to pull the current balance.
        ParseQuery<ParseObject> query = ParseQuery.getQuery("trial");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> object, ParseException e) {
                if (e == null) {
                    for (int i = 0; i < object.size(); i++) {
                        String trial = object.get(i).getString("trial");
                        String balanceString = valueOf(trial);
                        show.setText(balanceString);
                        Log.d("Transaction", "Balance: " + balanceString);
                    }
                }else{
                    Log.d("Transaction", e.getMessage());
                }
            }
        });
        //Queries data to get the most recent transaction made to pull object Id, credit , debit and date of transaction.
        ParseQuery<ParseObject> query1 = ParseQuery.getQuery("trial");
        query1.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> object, ParseException e) {
                if (e == null) {
                    for (int i = 0; i < object.size(); i++) {
                        String trial = object.get(i).getString("trial");
                        String credit = object.get(i).getString("credit");
                        String debit = object.get(i).getString("debit");
                        String objectId = object.get(i).getObjectId();
                        Date createdAt = object.get(i).getCreatedAt();
                        String balanceString = valueOf(trial);
                        show1.setText(credit);
                        show2.setText(debit);
                        String date = valueOf(createdAt);
                        show3.setText(date);
                        object1.setText(objectId);
                        Log.d("Transaction", "Balance: " + balanceString);
                    }
                }else{
                    Log.d("Transaction", e.getMessage());
                }
            }
        });

    }


}