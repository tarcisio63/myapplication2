package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;

// This is the Login Activity. Allows users to access on credentials
public class MainActivity extends AppCompatActivity {
    private Button button1;
    private EditText email;
    private EditText password;
    private TextView button;
    private TextView forgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.button);
        button1 = findViewById(R.id.button1);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        forgot = findViewById(R.id.forgot);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });
        //Login Activity to take place.
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailValue = email.getText().toString();
                String passwordValue = password.getText().toString();
               ParseUser.logInInBackground(emailValue, passwordValue, new LogInCallback() {
                   @Override
                   public void done(ParseUser user, ParseException e) {
                       if (user != null) {
                           //If the credentials return true, The User is allowed to progress to make transaction.
                           openActivity();
                       } else {
                           //If returns false, Sign error toasts.
                           Toast.makeText(getApplicationContext(),
                                   "Sign in Error", Toast.LENGTH_LONG)
                                   .show();
                        }
                    }
                });
            }
            });
        //Once proceeds to forgotten password Activity.
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });
            }
// these are methods to progress to the new Activity after they are called.
    private void openActivity3() {
        Intent intent = new Intent(this, MainActivity3.class);
        startActivity(intent);
    }

    private void openActivity() {
        Intent intent = new Intent(this, transaction.class);
        startActivity(intent);
    }


    private void openActivity2() {
        Intent intent = new Intent(this, MainActivity2.class);
        startActivity(intent);
    }
    }









